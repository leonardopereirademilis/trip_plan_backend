const config = require('config');
const mongoose = require('mongoose');
mongoose.connect(process.env.NODE_ENV === 'production' ? config.production.connectionString : config.development.connectionString, {useNewUrlParser: true});
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../src/users/user.model'),
    Trip: require('../src/trips/trip.model'),
    Item: require('../src/items/item.model'),
    Note: require('../src/notes/note.model')
};
