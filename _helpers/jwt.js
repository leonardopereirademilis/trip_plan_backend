const expressJwt = require('express-jwt');
const config = require('config');
const userService = require('../src/users/user.service');

module.exports = jwt;

function jwt() {
    const secret = process.env.NODE_ENV === 'production' ? config.production.secret : config.development.secret;

    return expressJwt({secret, isRevoked}).unless({
        path: [
            // public routes that don't require authentication
            '/users/authenticate',
            '/users/register',
            '/users/forgot-password',
            '/users/reset-password'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
}
