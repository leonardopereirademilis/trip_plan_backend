﻿const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const db = require('_helpers/db');
const User = db.User;
const tripService = require('../trips/trip.service');
const i18n = require("i18n");
const path = require('path');

const hbs = require('nodemailer-express-handlebars'),
    email = 'contato@gastosdeviagem.com.br',
    pass = '3dez82D?'
nodemailer = require('nodemailer');

const smtpTransport = nodemailer.createTransport({
    host: 'smtpout.secureserver.net',
    port: 80,// 3535, 25	465,
    auth: {
        user: email,
        pass: pass
    }
});

const handlebarsOptions = {
    viewEngine: {
        extname: '.html',
        layoutsDir: 'templates',
        partialsDir : 'partials'
    },
    viewPath: 'templates',
    extName: '.html'
};

smtpTransport.use('compile', hbs(handlebarsOptions));

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    forgotPassword,
    resetPassword
};

async function authenticate({username, password}) {
    const user = await User.findOne({username});
    if (user && bcrypt.compareSync(password, user.hash)) {
        const {hash, ...userWithoutHash} = user.toObject();

        const token = process.env.NODE_ENV === 'production' ? jwt.sign({sub: user.id}, config.production.secret) : jwt.sign({sub: user.id}, config.development.secret);

        return {
            ...userWithoutHash,
            token
        };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(userParam) {
    // validate
    if (await User.findOne({username: userParam.username})) {
        throw i18n.__('Username "') + userParam.username + i18n.__('" is already taken');
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    senduserCreatedEmail(user);

    // save user
    await user.save();
}

function senduserCreatedEmail(user) {
    const data = {
        to: email,
        from: i18n.__('Trip Plan') + ' <' + email + '>', // sender address
        template: 'user-created-email',
        subject: i18n.__('User Created'),
        context: {
            firstName: user.firstName,
            lastName: user.lastName,
            username: user.username,
            createdDate: user.createdDate
        }
    };

    return smtpTransport.sendMail(data, function (err) {
        if (!err) {
            return res.json({message: i18n.__('Password reset')});
        } else {
            throw err;
        }
    });
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw i18n.__('User not found');
    if (user.username !== userParam.username && await User.findOne({username: userParam.username})) {
        throw i18n.__('Username "') + userParam.username + i18n.__('" is already taken');
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    tripService.deleteAllByUserId(id);
    await User.findByIdAndRemove(id);
}


async function forgotPassword(userParam) {
    const user = await User.findOne({username: userParam.username});

    if (!user) {
        throw i18n.__('Username "') + userParam.username + i18n.__('" not found');
    }

    // create the random token
    let token = null;
    const buffer = crypto.randomBytes(20);
    token = buffer.toString('hex');

    if (!token) {
        throw i18n.__('Token error');
    }

    const url = process.env.NODE_ENV === 'production' ? config.production.url : config.development.url;

    await User.findByIdAndUpdate({_id: user._id}, {
        reset_password_token: token,
        reset_password_expires: Date.now() + 86400000
    }, {upsert: true, new: true});

    const data = {
        to: user.username,
        from: i18n.__('Trip Plan') + ' <' + email + '>', // sender address
        bcc: email,
        template: 'forgot-password-email',
        subject: i18n.__('Password help has arrived!'),
        context: {
            url: url + '/reset-password?token=' + token,
            name: i18n.__('Dear') + ' ' + user.firstName,
            username: user.username,
            forgot_password_body: i18n.__('You requested for a password reset for the user {{username}}, kindly use the link below to reset your password.', {username: user.username}),
            link_text: i18n.__('Password reset'),
            footer: i18n.__('Cheers!')
        }
    };

    await sendEmailWithTimeout(30000, data);
}

function sendEmailWithTimeout(delayms, data) {
    return new Promise(function (resolve, reject) {
        smtpTransport.sendMail(data, function (error, info) {
            if (error) {
                reject(i18n.__('Error sending the e-mail'));
            }
            resolve(i18n.__('Kindly check your email for further instructions'));

        });
        setTimeout(function () {
            reject(i18n.__('Error sending the e-mail'))
        }, delayms);
    });
}

async function resetPassword(userParam) {
    const user = await User.findOne({
        reset_password_token: userParam.token,
        reset_password_expires: {
            $gt: Date.now()
        }
    });

    if (user) {
        if (userParam.newPassword === userParam.verifyPassword) {
            user.hash = bcrypt.hashSync(userParam.newPassword, 10);
            user.reset_password_token = undefined;
            user.reset_password_expires = undefined;
            if (await user.save()) {
                const data = {
                    to: user.username,
                    from: i18n.__('Trip Plan') + ' <' + email + '>', // sender address
                    bcc: email,
                    template: 'reset-password-email',
                    subject: i18n.__('Password Reset Confirmation'),
                    context: {
                        name: i18n.__('Dear') + ' ' + user.firstName,
                        reset_password_body: i18n.__('Your password has been successful reset, you can now login with your new password.'),
                        footer: i18n.__('Cheers!')
                    }
                };

                return smtpTransport.sendMail(data, function (err) {
                    if (!err) {
                        return res.json({message: i18n.__('Password reset')});
                    } else {
                        throw err;
                    }
                });
            }
        } else {
            throw i18n.__('Passwords do not match')
        }
    } else {
        throw i18n.__('Password reset token is invalid or has expired')
    }
}
