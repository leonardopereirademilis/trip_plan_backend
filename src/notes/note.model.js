const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    note: {type: String},
    trip: {type: mongoose.Schema.Types.ObjectId, ref: 'Trip', required: true, unique: true},
    createdDate: {type: Date, default: Date.now}
});

schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Note', schema);