﻿const db = require('_helpers/db');
const Note = db.Note;
const Trip = db.Trip;
const i18n = require("i18n");

module.exports = {
    getAll,
    getById,
    getByTripId,
    deleteByTripId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Note.find().select('-hash');
}

async function getById(id) {
    return await Note.findById(id).select('-hash');
}

async function getByTripId(tripid) {
    const trip = await Trip.findById(tripid).select('-hash');
    return await Note.findOne({trip: trip});
}

async function deleteByTripId(tripid) {
    const trip = await Trip.findById(tripid).select('-hash');
    await Note.findOneAndRemove({trip: trip});
}

async function create(noteParam) {
    // validate
    if (await Note.findOne({trip: noteParam.trip})) {
        throw i18n.__('Note "') + noteParam.noteName + i18n.__('" is already taken');
    }

    const note = new Note(noteParam);

    // save note
    await note.save();
}

async function update(id, noteParam) {
    const note = await Note.findById(id);

    // validate
    if (!note) throw i18n.__('Note not found');

    // copy noteParam properties to note
    Object.assign(note, noteParam);

    await note.save();
}

async function _delete(id) {
    await Note.findByIdAndRemove(id);
}