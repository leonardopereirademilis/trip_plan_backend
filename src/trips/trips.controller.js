﻿const express = require('express');
const router = express.Router();
const tripService = require('./trip.service');

// routes
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.get('/user/:id', getAllByUserId);
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    tripService.getAll()
        .then(trips => res.json(trips))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    tripService.getById(req.trip.sub)
        .then(trip => trip ? res.json(trip) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    tripService.getById(req.params.id)
        .then(trip => trip ? res.json(trip) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByUserId(req, res, next) {
    tripService.getAllByUserId(req.params.id)
        .then(trip => trip ? res.json(trip) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    tripService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    tripService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    tripService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}