﻿const db = require('_helpers/db');
const User = db.User;
const Trip = db.Trip;
const Note = db.Note;
const i18n = require("i18n");

const noteService = require('../notes/note.service');
const itemService = require('../items/item.service');

module.exports = {
    getAll,
    getById,
    getAllByUserId,
    deleteAllByUserId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Trip.find().select('-hash');
}

async function getById(id) {
    return await Trip.findById(id).select('-hash');
}

async function getAllByUserId(userid) {
    const user = await User.findById(userid).select('-hash');
    return await Trip.find({user: user});
}

async function deleteAllByUserId(userid) {
    const user = await User.findById(userid).select('-hash');
    await Trip.remove({user: user});
}

async function create(tripParam) {
    // validate
    if (await Trip.findOne({tripName: tripParam.tripName, user: tripParam.user})) {
        throw i18n.__('Trip Name "') + tripParam.tripName + i18n.__('" is already taken');
    }

    const trip = new Trip(tripParam);

    // save trip
    if (await trip.save()) {
        const note = new Note({trip: trip});
        await note.save();
    }
}

async function update(id, tripParam) {
    const trip = await Trip.findById(id);

    // validate
    if (!trip) throw i18n.__('Trip not found');
    if (trip.tripName !== tripParam.tripName && await Trip.findOne({
        tripName: tripParam.tripName,
        user: tripParam.user
    })) {
        throw i18n.__('Trip Name "') + tripParam.tripName + i18n.__('" is already taken');
    }

    // copy tripParam properties to trip
    Object.assign(trip, tripParam);

    await trip.save();
}

async function _delete(id) {
    itemService.deleteAllByTripId(id);
    noteService.deleteByTripId(id);
    await Trip.findByIdAndRemove(id);
}