const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    tripName: {type: String, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    tripDateStart: {type: Date},
    tripDateEnd: {type: Date},
    createdDate: {type: Date, default: Date.now}
});

schema.index({tripName: 1, user: 1}, {unique: true});
schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Trip', schema);