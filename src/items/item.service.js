﻿const db = require('_helpers/db');
const Item = db.Item;
const Trip = db.Trip;
const i18n = require("i18n");

module.exports = {
    getAll,
    getById,
    getAllByTripId,
    deleteAllByTripId,
    getAllByTripIdAndItemType,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await Item.find().select('-hash');
}

async function getById(id) {
    return await Item.findById(id).select('-hash');
}

async function getAllByTripId(tripid) {
    const trip = await Trip.findById(tripid).select('-hash');
    return await Item.find({trip: trip});
}

async function deleteAllByTripId(tripid) {
    const trip = await Trip.findById(tripid).select('-hash');
    await Item.remove({trip: trip});
}

async function getAllByTripIdAndItemType(tripid, itemType) {
    const trip = await Trip.findById(tripid).select('-hash');
    return await Item.find({trip: trip, itemType: itemType}).sort({itemOk: -1, itemDate: 1, createdDate: 1});
}

async function create(itemParam) {
    const item = new Item(itemParam);

    // save item
    await item.save();
}

async function update(id, itemParam) {
    const item = await Item.findById(id);

    // validate
    if (!item) throw i18n.__('Item not found');

    // copy itemParam properties to item
    Object.assign(item, itemParam);

    await item.save();
}

async function _delete(id) {
    await Item.findByIdAndRemove(id);
}