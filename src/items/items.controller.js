﻿const express = require('express');
const router = express.Router();
const itemService = require('./item.service');

// routes
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.get('/trip/:id', getAllByTripId);
router.get('/trip/:id/:itemType', getAllByTripIdAndItemType);
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    itemService.getAll()
        .then(items => res.json(items))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    itemService.getById(req.item.sub)
        .then(item => item ? res.json(item) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    itemService.getById(req.params.id)
        .then(item => item ? res.json(item) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByTripId(req, res, next) {
    itemService.getAllByTripId(req.params.id)
        .then(item => item ? res.json(item) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByTripIdAndItemType(req, res, next) {
    itemService.getAllByTripIdAndItemType(req.params.id, req.params.itemType)
        .then(item => item ? res.json(item) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    itemService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    itemService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    itemService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}