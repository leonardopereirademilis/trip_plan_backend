const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    itemType: {type: String, required: true},
    itemName: {type: String, required: true},
    trip: {type: mongoose.Schema.Types.ObjectId, ref: 'Trip', required: true},
    value: {type: Number, required: true},
    exchangeValue: {type: Number},
    selectedCurrency: {type: String},
    times: {type: Number, required: true, min: 1},
    itemDate: {type: Date},
    itemOk: {type: Boolean, required: true, default: false},
    createdDate: {type: Date, default: Date.now}
});

// schema.index({itemName: 1, itemType: 1, trip: 1}, {unique: true});
schema.set('toJSON', {virtuals: true});

module.exports = mongoose.model('Item', schema);