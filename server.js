﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');
const i18n = require('i18n');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/users', require('./src/users/users.controller'));
app.use('/trips', require('./src/trips/trips.controller'));
app.use('/items', require('./src/items/items.controller'));
app.use('/notes', require('./src/notes/notes.controller'));

// global error handler
app.use(errorHandler);

i18n.configure({
    locales: ['en', 'pt'],
    directory: __dirname + '/locales',
    defaultLocale: 'pt',
});

const port = process.env.NODE_BACKEND_PORT ? process.env.NODE_BACKEND_PORT : 3000;
const server = app.listen(port, function (req, res) {
    console.log('Server listening on port ' + port);
});
